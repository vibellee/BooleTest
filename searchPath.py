from LbConfiguration.SP2.options import (SearchPath, SearchPathEntry,
                                         EnvSearchPathEntry, NightlyPathEntry,
                                         LHCbDevPathEntry)

path = SearchPath([EnvSearchPathEntry('User_release_area', '/afs/cern.ch/user/v/vibellee/cmtuser'), EnvSearchPathEntry('CMTPROJECTPATH', '/afs/cern.ch/user/v/vibellee/cmtuser:/afs/cern.ch/lhcb/software/releases:/afs/cern.ch/sw/Gaudi/releases:/afs/cern.ch/sw/lcg/releases:/afs/cern.ch/sw/lcg/app/releases'), EnvSearchPathEntry('LHCBPROJECTPATH', '/afs/cern.ch/lhcb/software/releases:/afs/cern.ch/sw/Gaudi/releases:/afs/cern.ch/sw/lcg/releases:/afs/cern.ch/sw/lcg/app/releases'), EnvSearchPathEntry('CMAKE_PREFIX_PATH', '/afs/cern.ch/lhcb/software/releases/LBSCRIPTS/LBSCRIPTS_v8r6p7/LbRelease/data/DataPkgEnvs:/afs/cern.ch/lhcb/software/releases/LBSCRIPTS/LBSCRIPTS_v8r6p7/LbUtils/cmake')])
